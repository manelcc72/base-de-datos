package com.cabezas.basedatos;

/**
 * Created by manel on 22/09/2015.
 */
public class Peliculas {
    int id;
    String title;
    String genre;
    int image;

    public Peliculas() {
    }

    public Peliculas( String title, String genre, int image) {
        this.title = title;
        this.genre = genre;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
