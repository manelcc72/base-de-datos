package com.cabezas.basedatos;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class MySQLiteHelper extends SQLiteOpenHelper {

    // Versión de la base de datos
    private static final int DATABASE_VERSION = 1;
    // Nombre de la base de datos
    private static final String DATABASE_NAME = "PeliculaDB";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Sentencia SQL de creación de la tabla peliculas
        String CREATE_PELICULA_TABLE = "CREATE TABLE peliculas ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "title TEXT, "+
                "genre TEXT, "+
                "image INTEGER )";

        // Crear tabla "peliculas"
        db.execSQL(CREATE_PELICULA_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Borrar tablas antiguas si existen
        db.execSQL("DROP TABLE IF EXISTS peliculas");

        // Crear tabla peliculas nueva
        this.onCreate(db);
    }
    //---------------------------------------------------------------------

    /**
     * Completar las operaciones CRUD de Pelicula
     */

    // Nombre de la tabla de Peliculas
    private static final String TABLE_PELICULAS = "peliculas";

    // Nombres de las columnas de la tabla de Peliculas
    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_GENRE = "genre";
    private static final String KEY_IMAGE_URL = "image";

    private static final String[] COLUMNS = {KEY_ID,KEY_TITLE,KEY_GENRE,KEY_IMAGE_URL};

    public void addPelicula(Peliculas pelicula){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values=new ContentValues();
        values.put(KEY_TITLE,pelicula.getTitle());
        values.put(KEY_GENRE, pelicula.getGenre());
        values.put(KEY_IMAGE_URL, pelicula.getImage());

        db.insert(TABLE_PELICULAS,null,values);


        //TODO Insertar una Pelicula en la BD

        db.close();
    }

    public Peliculas getPelicula(int id){

        Peliculas pelicula = null;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor=db.query(TABLE_PELICULAS,COLUMNS,"id=?",new String[]{String.valueOf(id)},null,null,null,null);
        if (cursor!=null){
            cursor.moveToFirst();
            pelicula=new Peliculas();
            pelicula.setId(Integer.parseInt(cursor.getString(0)));
            //Crear con el resto de atributos
        }
        //TODO Obtener una Pelicula

        db.close();

        return pelicula;
    }

    public List<Peliculas> getAllPeliculas() {
        List<Peliculas> peliculas = new ArrayList<Peliculas>();

        SQLiteDatabase db = this.getWritableDatabase();
        String query="SELECT * FROM "+TABLE_PELICULAS;

        Cursor cursor=db.rawQuery(query,null);
        Peliculas pelicula=null;
        if (cursor.moveToFirst()){
            do{
                pelicula=new Peliculas();
                pelicula.setId(Integer.parseInt(cursor.getString(0)));
                pelicula.setTitle(cursor.getString(1));
                pelicula.setGenre(cursor.getString(2));
                pelicula.setImage(Integer.parseInt(cursor.getString(3)));
                peliculas.add(pelicula);

            }while (cursor.moveToNext());
        }
        //TODO Obtener todas las Peliculas

        db.close();

        return peliculas;
    }

    public void updatePelicula(Peliculas pelicula) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values=new ContentValues();
        values.put("title",pelicula.getTitle());
        values.put("author",pelicula.getGenre());

        db.update(TABLE_PELICULAS,
                values,
                KEY_ID+" = ?",
                new String[]{String.valueOf(pelicula.getId())});

        //TODO Actualizar una Pelicula

        db.close();



    }

    public void deletePelicula(Peliculas pelicula) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_PELICULAS,
                KEY_ID+" = ?",
                new String[]{String.valueOf(pelicula.getId())});
        //TODO Borrar una Pelicula

        db.close();


    }

    public void deleteAllPeliculas(){


        SQLiteDatabase db = this.getWritableDatabase();
db.execSQL("DELETE FROM "+TABLE_PELICULAS);
        //TODO Borrar todas las Peliculas

        db.close();
    }
}