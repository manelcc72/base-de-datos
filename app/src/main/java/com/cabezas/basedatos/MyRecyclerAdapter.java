package com.cabezas.basedatos;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;


public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

    private List<Peliculas> items;
    private int itemLayout;
    private Context context;

    public MyRecyclerAdapter(Context context, List<Peliculas> items, int itemLayout) {
        this.context = context;
        this.items = items;
        this.itemLayout = itemLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Peliculas item = items.get(position);

        holder.title.setText(item.getTitle());
        holder.genre.setText(item.getGenre());
        holder.image.setImageResource(item.getImage());
        holder.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               //TODO Ampliación
            }
        });
        holder.itemView.setTag(item);
    }


    public void add(Peliculas item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Peliculas item) {
        int position = items.indexOf(item);
        items.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView title;
        public TextView genre;
        public Button info;

        public ViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.imageViewRow);
            title = (TextView) itemView.findViewById(R.id.textViewRowTitle);
            genre = (TextView) itemView.findViewById(R.id.textViewRowDescription);
            info = (Button) itemView.findViewById(R.id.buttonRowInfo);
        }
    }
}
